#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""EJERCICIO 5"""
import random


# clase de datos del perro
class perro():
    # llegan datos ingresados
    def setperro(self, nombre, color, raza, edad):
        self.nombre = nombre
        self.color = color
        self.raza = raza
        self.edad = edad
    # este dato es a parte para mayor comodidad

    def set_accion(self, genero):
        # lista de acciones random
        acciones = ("ladrando", "jugando", "comiendo",
                    "peleando por terreno", "haciendo caca")
        # cuando el perro sea macho este peleará por terreno
        if genero == "m":
            accion = "peleando por terreno"
        else:
            # sino, se escogerá una acción random
            accion = random.choice(acciones)
        self.accion = accion
    # funciones que envian los datos

    def getnombre(self):
        return self.nombre

    def getcolor(self):
        return self.color

    def getraza(self):
        return self.raza

    def getedad(self):
        return self.edad

    def getaccion(self):
        return self.accion

# función principal
if __name__ == '__main__':
    try:
        # variable de clase
        dog = perro()
        print(" SU PERRO")
        nombre = input(" -Nombre de su perro: ")
        color = input(" -Color de su perro (en general): ")
        raza = input(" -Raza de su perro: ")
        edad = input(" -Edad de su perro: ")
        # se ingresan y envían datos
        dog.setperro(nombre, color, raza, edad)
        genero = input(" -Género de su perro (h = hembra/m = macho): ")
        while genero != "m" and genero != "h":
            print(" INGRESE VALORES VÁLIDOS:")
            genero = input(" -Género de su perro (h = hembra/m = macho): ")
        if genero == "m" or genero == "h":
            dog.set_accion(genero)
        # se traen los datos
        print("   Su perro", dog.getnombre())
        print("   De color", dog.getcolor())
        print("   Es de raza", dog.getraza())
        print("   Tiene", dog.getedad(), "años de edad")
        print("   Está", dog.getaccion())
    # los datos ingresados deben ser válidos
    except ValueError:
        print(" INGRESE VALORES VÁLIDOS")
