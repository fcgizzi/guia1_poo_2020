#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""EJERCICIO 1
    1. Implemente la clase Tarjeta que permita manipular (get & set) la siguiente informaciÓn:
    - Nombre del titular
    - Saldo de la tarjeta
"""


# clase de tarjeta en la que llegan los datos solicitados
class tarjeta():
    # datos que se reciben
    def setnombre(self, titular):
        self.nombre = titular

    def setsaldo(self, saldo):
        self.saldo = saldo

    # datos que se envían
    def getnombre(self):
        return self.nombre

    def getsaldo(self):
        return self.saldo

# menú
if __name__ == "__main__":
    # se define la clase como variable para sus uso
    tarjet = tarjeta()
    # se pide ingresar el nombre del titular, la cantidad y el gasto
    titular = input(" -Ingresar nombre de titular: ")
    tarjet.setnombre(titular)
    dinero = int(input(" -Ingresar saldo de tarjeta: "))
    tarjet.setsaldo(dinero)

    compra = input(" ¿Desea hacer una compra?(s/n): ")
    # mientras el valor ingresado no sea lo que se pide, este será un bucle
    while compra != "s" and compra != "n":
        compra = input(" Valor inválido ¿desea hacer una compra?(s/n): ")
    # si no se compra nada, finaliza el programa con el saldo sin gastar
    if compra == "n":
        print(" ¡Entendido!")
        print(" Titular: ", tarjet.getnombre())
        print(" Saldo sin gastar: $", tarjet.getsaldo())
        exit()
    # de lo contrario, pide el monto que desea gastar
    elif compra == "s":
        gasto = int(input(" ¿Cuánto desea gastar?: "))
        # el gasto debe ser menor o igual al dinero que se tiene
        while gasto > dinero:
            print(" Lamentablemente no cuentas con esa cantidad")
            gasto = int(input(" Ingresa nuevo monto : "))
        # de esa manera, este se resta del dinero y se obtiene el nuevo saldo
        if gasto <= dinero:
            saldo = dinero - gasto
    # nuevo saldo enviado a la calse
    tarjet.setsaldo(saldo)

    # imprime datos
    print("\n   |RESUMEN DE DATOS|\n ")
    print(" -Titular: ", tarjet.getnombre())
    print(" -Saldo principal: $", dinero)
    print(" -Gasto realizado: $", gasto)
    print(" -Saldo actual : $", tarjet.getsaldo())
