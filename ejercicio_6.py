#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""EJERCICIO 6"""


# clase de la cuenta banacria
class cuenta():
    # llegan 3 distintos datos
    def setsaldo(self, saldo):
        self.saldo = saldo

    def setingreso(self, ingreso):
        saldo = self.saldo + ingreso
        self.saldo = saldo

    def setgiro(self, giro):
        saldo = self.saldo - giro
        self.saldo = saldo

    def settransferencia(self, trans):
        saldo = self.saldo - trans
        self.saldo = saldo
    # pero solo se envía uno
    # se usó uno par ahorrar lineas

    def getsaldo(self):
        return self.saldo


# función que contiene las opciones disponibles
def menu():
    # se deben ingresar datos válidos
    try:
        # se debe ingresar una opción válida o entrará en un bucle
        opcion = int(input("-->"))
        while opcion != 1 and opcion != 2 and opcion != 3 and opcion != 4:
            print(" ERROR//INGRESE VALOR VÁLIDO")
            opcion = int(input("-->"))
        # para opción 1, se ingresan datos validos de ingreso
        if opcion == 1:
            print("---------------------------------------")
            ingreso = int(input(" Monto que desea ingresar: $"))
            while ingreso < 0:
                print(" Debe ingresar dinero")
                print("---------------------------------------")
                ingreso = int(input(" Monto que desea ingresar: $"))
            if ingreso > 0:
                # este debe ser positivo
                # asi se enviará a la clase
                account.setingreso(ingreso)
                print("---------------------------------------")
            # imprime nuevo saldo y vuelve a las opciones
            print(" Excelente, su nuevo saldo es: $", account.getsaldo())
            return menu()
        # opción 2 para transferir valores válidos
        elif opcion == 2:
            print("---------------------------------------")
            trans = int(input(" Monto que desea transferir: $"))
            while trans < 0:
                print(" Eso no es posible")
                print("---------------------------------------")
                trans = int(input(" Monto que desea transferir: $"))
            while trans > account.getsaldo():
                print(" No posee esa cantidad, ingrese un valor menor")
                print("---------------------------------------")
                trans = int(input(" Monto que desea transferir: $"))
            # el monto debe ser positivo y menor al saldo que se tiene
            # asi será enviado a la clase
            if trans < account.getsaldo():
                account.settransferencia(trans)
            # luego se solicita el destinatario
            quien = int(input(" Ingrese cuenta a la que desa transferir: "))
            # luego imprime todos los datos
            print("---------------------------------------")
            print(" Excelente, Ud transfirió a: ", quien)
            print(" La cantidad de: $", trans)
            print(" Su nuevo saldo es: $", account.getsaldo())
            # vuelve a las opciones
            return menu()
        # opción exactamente igual a la 2 pero distintas variables
        elif opcion == 3:
            print("---------------------------------------")
            giro = int(input(" Monto que desea girar: $"))
            while giro < 0:
                print(" Eso no es posible")
                print("---------------------------------------")
                giro = int(input(" Monto que desea girar: $"))
            while giro > account.getsaldo():
                print(" No posee esa cantidad, ingrese un valor menor")
                print("---------------------------------------")
                giro = int(input(" Monto que desea girar: $"))
            if giro < account.getsaldo():
                account.setgiro(giro)
            print("---------------------------------------")
            print(" Excelente, Ud retiró: $", giro)
            print(" Su nuevo saldo es: $", account.getsaldo())
            return menu()
        # al escoger la opción 4 el programa finaliza
        elif opcion == 4:
            print("---------------------------------------")
            print("               ADIÓS")
            print("---------------------------------------")
            exit()
    # se deben ingresar valores válidos
    except ValueError:
        print("\n INGRESE OPCIONES VÁLIDAS")
        menu()

# función principal
if __name__ == '__main__':
    # variable de la clase
    account = cuenta()
    print(" CUENTA BANCARIA")
    # se ingresa nombre para hacerlo más dinámico
    nombre = input(" Ingrese su nombre: ")
    n_cuenta = "3235 7728 6182 2829"
    saldo = 300000
    account.setsaldo(saldo)

    # se imprime la información
    # y se llama la menú
    print("---------------------------------------")
    print(" Ingreso exitoso\n")
    print(" - Cliente:", nombre)
    print(" - N° de cuenta:", n_cuenta)
    print(" - Dinero en tarjeta: $", account.getsaldo())
    print("\n---------------------------------------")
    print(" OPCIONES")
    print(" 1) Ingresar dinero")
    print(" 2) Transferir dinero")
    print(" 3) Girar dinero")
    print(" 4) salir")
    print("---------------------------------------")
    menu()
