#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""EJERCICIO 4

"""


# clase con datos de amigos
class amigos():
    # función con todos los calculos de los precios
    def setprecios(self, precio, grupo):
        # cada dato aporvecha de enviarse a un get
        self.iva = precio * 0.19
        self.tips = precio * 0.1
        self.total = (precio * 0.1) + (precio * 0.19) + precio
        self.cadauno = precio / grupo

    # funciones que enviarán los datos
    def getcadauno(self):
        return self.cadauno

    def getiva(self):
        return self.iva

    def gettips(self):
        return self.tips

    def gettotal(self):
        return self.total

# función principal
if __name__ == '__main__':
    print(" Vamo a pagar la cuenta")
    try:
        # cariable para clase
        friend = amigos()
        cadauno = 0
        # se ingresan datos, estos deben ser numeros
        precio = int(input(" ¿Cuánto es el total de la boleta?: "))
        grupo = int(input(" ¿Cuántas personas pagarán?: "))
        # se envían a la clase
        friend.setprecios(precio, grupo)
        print(" ¿Cuánto tiene cada uno?")
        amigo = 1
        # se ingresa la cantidad que tiene cada amigo
        for i in range(grupo):
            print(amigo, "-amigo: ")
            cada = int(input(" $"))
            cadauno = cadauno + cada
            amigo += 1
        # la cantidad que tienen no debe ser menor al precio total
        if cadauno < precio:
            print(" NO ALCANZA, MOMENTO PATIPERRO")
        # si alcanza, se mostrarán los resultados
        else:
            print(" - Cuentan con:", cadauno, "$")
            print(" - El iva es de:", friend.getiva(), "$")
            print(" - La propina es de:", friend.gettips(), "$")
            print(" - El total de pago es de:", friend.gettotal(), "$")
            print(" - Cada uno debe pagar:", friend.getcadauno(), "$")
    except ValueError:
        print(" INGRESE VALORES VÁLIDOS")
