#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" EJERCICIO 3 """


class algoritmo():

    # el número es transformado a lista
    def suma(self, numero):
        lista_numero = []
        for i in str(numero):
            lista_numero.append(int(i))
        self.lista_numero = lista_numero
        # se entrega la lista en la misma clase
        algoritmo.parte_2(self.lista_numero)

    # función sumatoria
    def parte_2(self, lista_numero):
        # se crea una lista temporal
        lista_temp = []
        suma = 0
        # se obtienen números de a uno
        for i in range(len(lista_numero)):
            suma = int((suma + lista_numero[i]) % 10)
            # y se agregan en la lsta temmporal
            lista_temp.append(suma)
        self.lista_nueva = lista_temp
        # se entrega la nueva lista a la clase
        # y es enviado para que se le sume 7 a cada digito
        algoritmo.sumar7(self.lista_nueva)

    # función suma 7 a cada digito
    def sumar7(self, lista_nueva):
        # se crea una lista temporal
        lista_temp = []
        # recorre la lista y suma 7
        for i in range(len(lista_nueva)):
            lista_temp.append((7 + lista_nueva[i]) % 10)
        self.lista_sumada = lista_temp
        # se envían los datos a otro sector de la clase
        algoritmo.intercambio(self.lista_sumada)

    # función cambia los digitos de posición
    def intercambio(self, lista_sumada):
        # se crea temporal
        lista_temp = 0
        # se recorre la lista para invertirla y correrla 3 espacios
        for i in range(len(lista_sumada)):
            lista_temp += (lista_sumada[3 - i]) * (10 ** i)
        # se envían los datos a otra sección de la clase
        self.finalizado = lista_temp

    # aquí simplemente se finaiza el proceso dandole un get a la ultima lista
    def getfinalizado(self):
        return self.finalizado

# función principal
if __name__ == '__main__':
    # debe responder lo que se solicita
    try:
        numero = int(input(" Ingrese número de 4 digitos para transformar: "))
        # el numero debe cumplir con la siguiente condición
        if 10000 > numero > 999:
            # se le da valor a la clase
            algoritmo = algoritmo()
            # se envían los datos a la clase
            algoritmo.suma(numero)
            # se resive el último dato
            print("\n Su clave secreta es: ", algoritmo.getfinalizado())
        else:
            print(" Debe ingresar 4 digitos")
    except ValueError:
        print("\n La clave solo es efectiva con números")

