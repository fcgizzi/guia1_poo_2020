#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" EJERCICIO 2
    Crea una clase llamada Libro que guarde la información de cada uno de los libros de una biblioteca.
    La clase debe guardar el título del libro, autor, número total de ejemplares del libro y número de
    ejemplares prestados. La clase contendrá los siguientes métodos:
    - Constructor con parámetros para la clase libro.
    - Constructor por defecto para la clase biblioteca.
    - Métodos Set/Get para los atributos privados
"""

# clase de datos sobre el libro
class libro():
    # datos que se reciben
    def setnombre(self, nombre):
        self.nombre = nombre

    def setautor(self, autor):
        self.autor = autor

    # datos que se envían
    def getnombre(self):
        return self.nombre

    def getautor(self):
        return self.autor

# clase de datos sobre cantidades relacionados al libro
class biblioteca():
    # datos que se reciben
    def setcantidad(self, cantidad):
        self.cantidad = cantidad

    def setprestado(self, prestado):
        self.prestado = prestado

    # datos que se envían
    def getcantidad(self):
        return self.cantidad

    def getprestado(self):
        return self.prestado

# menú de ingreso de datos
def menu():
    # variables para uso de clases
    book = libro()
    library = biblioteca()
    
    # se ingresan datos y son enviados a las clases
    # usando "set" y ek nombre de la variable enviado
    # a la clase indicada
    name = input(" Ingresar nombre: ")
    book.setnombre(name)
    autor = input(" Ingresar autor: ")
    book.setautor(autor)
    many = input(" Ingresar ejemplares existentes: ")
    library.setcantidad(many)
    borrowed = input(" Ingresar cantidad de ejemplares prestados: ")
    library.setprestado(borrowed)
    
    # se imprimen los datos guardados
    # se traen los datos correspondientes con "get" y lo solicitado
    print("\n DATOS GUARDADOS")
    print(" - Titulo: ", book.getnombre())
    print(" - Autor: ", book.getautor())
    print(" - Ejemplares: ", library.getcantidad())
    print(" - Prestados: ", library.getprestado())

    # opción para agregar otro
    opcion = input("\n ¿Desea agregar otro?(s/n): ")
    # mientras no sea lo que se pide, pedirá de nuevo
    while opcion != "n" and opcion != "s":
        print(" //ERROR//")
        opcion = input(" ¿Desea agregar otro?(s/n): ")
    if opcion == "n":
        print("  OK...\n  ADIÓS")
        exit()
    elif opcion == "s":
        # si la opción es afirmativa, se reinicia el menú
        return menu()

    # menú principal
if __name__ == '__main__':
    print("\n  BIBLIOTECA uwu")
    # opción para guardar libro
    opcion = input(" ¿Desea guardar un libro?(s/n): ")
    while opcion != "n" and opcion != "s":
        print(" //ERROR//")
        opcion = input(" ¿Desea guardar un libro?(s/n): ")
    if opcion == "n":
        print("  OK...\n  ADIÓS")
        exit()
    elif opcion == "s":
        menu()

